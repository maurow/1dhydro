figure
plot(t/para.day, tot_source, 'r');
hold;
plot(t/para.day, -Q(1,:));
legend('lake filling rate', 'outflow', 'location', 'northwest');
xlabel('time (d)')
ylabel('Q (m^3/s)')
xlim([300,600])